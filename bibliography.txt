A. Mondal, M. Dey, D. Das, S. Nagpal, K. Garda, 2018. Chatbot: An automated conversation system for
the educational domain, in: 2018 International Joint Symposium on Artificial Intelligence and
Natural Language Processing (ISAI-NLP). Presented at the 2018 International Joint Symposium
on Artificial Intelligence and Natural Language Processing (iSAI-NLP), pp. 1–5.
https://doi.org/10.1109/iSAI-NLP.2018.8692927
A. Yorita, S. Egerton, C. Chan, N. Kubota, 2020. Chatbot for Peer Support Realization based on Mutual
Care, in: 2020 IEEE Symposium Series on Computational Intelligence (SSCI). Presented at the
2020 IEEE Symposium Series on Computational Intelligence (SSCI), pp. 1601–1606.
https://doi.org/10.1109/SSCI47803.2020.9308277
B. R. Ranoliya, N. Raghuwanshi, S. Singh, 2017. Chatbot for university related FAQs, in: 2017
International Conference on Advances in Computing, Communications and Informatics
(ICACCI). Presented at the 2017 International Conference on Advances in Computing,
Communications and Informatics (ICACCI), pp. 1525–1530.
https://doi.org/10.1109/ICACCI.2017.8126057
C. Lee, T. Chen, L. Chen, P. Yang, R. T. Tsai, 2018. Automatic Question Generation from Children’s
Stories for Companion Chatbot, in: 2018 IEEE International Conference on Information Reuse
and Integration (IRI). Presented at the 2018 IEEE International Conference on Information Reuse
and Integration (IRI), pp. 491–494. https://doi.org/10.1109/IRI.2018.00078
D. I. Sensuse, V. Dhevanty, E. Rahmanasari, D. Permatasari, B. E. Putra, J. S. Lusa, M. Misbah, P. Prima,
2019. Chatbot Evaluation as Knowledge Application: a Case Study of PT ABC, in: 2019 11th
International Conference on Information Technology and Electrical Engineering (ICITEE).
Presented at the 2019 11th International Conference on Information Technology and Electrical
Engineering (ICITEE), pp. 1–6. https://doi.org/10.1109/ICITEED.2019.8929967
E. Amer, A. Hazem, O. Farouk, A. Louca, Y. Mohamed, M. Ashraf, 2021. A Proposed Chatbot
Framework for COVID-19, in: 2021 International Mobile, Intelligent, and Ubiquitous Computing
Conference (MIUCC). Presented at the 2021 International Mobile, Intelligent, and Ubiquitous
Computing Conference (MIUCC), pp. 263–268.
https://doi.org/10.1109/MIUCC52538.2021.9447652
F. Bagwan, R. Phalnikar, S. Desai, 2021. Artificially Intelligent Health Chatbot Using Deep Learning, in:
2021 2nd International Conference for Emerging Technology (INCET). Presented at the 2021 2nd
International Conference for Emerging Technology (INCET), pp. 1–5.
https://doi.org/10.1109/INCET51464.2021.9456195
G. K. Vamsi, A. Rasool, G. Hajela, 2020. Chatbot: A Deep Neural Network Based Human to Machine
Conversation Model, in: 2020 11th International Conference on Computing, Communication and
Networking Technologies (ICCCNT). Presented at the 2020 11th International Conference on
Computing, Communication and Networking Technologies (ICCCNT), pp. 1–7.
https://doi.org/10.1109/ICCCNT49239.2020.9225395
K. Pathak, A. Arya, 2019. A Metaphorical Study Of Variants Of Recurrent Neural Network Models For
A Context Learning Chatbot, in: 2019 4th International Conference on Information Systems and
Computer Networks (ISCON). Presented at the 2019 4th International Conference on Information
Systems and Computer Networks (ISCON), pp. 768–772.
https://doi.org/10.1109/ISCON47742.2019.9036167
K. Ralston, Y. Chen, H. Isah, F. Zulkernine, 2019. A Voice Interactive Multilingual Student Support
System using IBM Watson, in: 2019 18th IEEE International Conference On Machine Learning
And Applications (ICMLA). Presented at the 2019 18th IEEE International Conference On
Machine Learning And Applications (ICMLA), pp. 1924–1929.
https://doi.org/10.1109/ICMLA.2019.00309
M. Casillo, F. Colace, L. Fabbri, M. Lombardi, A. Romano, D. Santaniello, 2020. Chatbot in Industry 4.0:
An Approach for Training New Employees, in: 2020 IEEE International Conference on Teaching,
Assessment, and Learning for Engineering (TALE). Presented at the 2020 IEEE International
Conference on Teaching, Assessment, and Learning for Engineering (TALE), pp. 371–376.
https://doi.org/10.1109/TALE48869.2020.9368339
M. Ganesan, D. C., H. B., K. A.S., L. B., 2020. A Survey on Chatbots Using Artificial Intelligence, in:
2020 International Conference on System, Computation, Automation and Networking (ICSCAN).
Presented at the 2020 International Conference on System, Computation, Automation and
Networking (ICSCAN), pp. 1–5. https://doi.org/10.1109/ICSCAN49426.2020.9262366

M. Jaiwai, K. Shiangjen, S. Rawangyot, S. Dangmanee, T. Kunsuree, A. Sa-nguanthong, 2021.
Automatized Educational Chatbot using Deep Neural Network, in: 2021 Joint International
Conference on Digital Arts, Media and Technology with ECTI Northern Section Conference on
Electrical, Electronics, Computer and Telecommunication Engineering. Presented at the 2021
Joint International Conference on Digital Arts, Media and Technology with ECTI Northern
Section Conference on Electrical, Electronics, Computer and Telecommunication Engineering, pp.
5–8. https://doi.org/10.1109/ECTIDAMTNCON51128.2021.9425716
M. Kosugi, O. Uchida, 2019. Chatbot Application for Sharing Disaster-information, in: 2019 International

Conference on Information and Communication Technologies for Disaster Management (ICT-
DM). Presented at the 2019 International Conference on Information and Communication

Technologies for Disaster Management (ICT-DM), pp. 1–2. https://doi.org/10.1109/ICT-
DM47966.2019.9032901

M. Nuruzzaman, O. K. Hussain, 2018. A Survey on Chatbot Implementation in Customer Service

Industry through Deep Neural Networks, in: 2018 IEEE 15th International Conference on E-
Business Engineering (ICEBE). Presented at the 2018 IEEE 15th International Conference on e-
Business Engineering (ICEBE), pp. 54–61. https://doi.org/10.1109/ICEBE.2018.00019

M. S. I. Bhuiyan, A. Razzak, M. S. Ferdous, M. J. M. Chowdhury, M. A. Hoque, S. Tarkoma, 2021.
BONIK: A Blockchain Empowered Chatbot for Financial Transactions, in: 2020 IEEE 19th
International Conference on Trust, Security and Privacy in Computing and Communications
(TrustCom). Presented at the 2020 IEEE 19th International Conference on Trust, Security and
Privacy in Computing and Communications (TrustCom), pp. 1079–1088.
https://doi.org/10.1109/TrustCom50675.2020.00143
M. W. Ashfaque, S. Tharewal, S. Iqhbal, C. N. Kayte, 2020. A Review on Techniques, Characteristics
and approaches of an intelligent tutoring Chatbot system, in: 2020 International Conference on
Smart Innovations in Design, Environment, Management, Planning and Computing
(ICSIDEMPC). Presented at the 2020 International Conference on Smart Innovations in Design,
Environment, Management, Planning and Computing (ICSIDEMPC), pp. 258–262.
https://doi.org/10.1109/ICSIDEMPC49020.2020.9299583
N. P. Patel, D. R. Parikh, D. A. Patel, R. R. Patel, 2019. AI and Web-Based Human-Like Interactive
University Chatbot (UNIBOT), in: 2019 3rd International Conference on Electronics,
Communication and Aerospace Technology (ICECA). Presented at the 2019 3rd International
conference on Electronics, Communication and Aerospace Technology (ICECA), pp. 148–150.
https://doi.org/10.1109/ICECA.2019.8822176
N. Rosruen, T. Samanchuen, 2018. Chatbot Utilization for Medical Consultant System, in: 2018 3rd

Technology Innovation Management and Engineering Science International Conference (TIMES-
ICON). Presented at the 2018 3rd Technology Innovation Management and Engineering Science

International Conference (TIMES-iCON), pp. 1–5. https://doi.org/10.1109/TIMES-
iCON.2018.8621678

N. Siangchin, T. Samanchuen, 2019. Chatbot Implementation for ICD-10 Recommendation System, in:
2019 International Conference on Engineering, Science, and Industrial Applications (ICESI).
Presented at the 2019 International Conference on Engineering, Science, and Industrial
Applications (ICESI), pp. 1–6. https://doi.org/10.1109/ICESI.2019.8863009
P. R. Telang, A. K. Kalia, M. Vukovic, R. Pandita, M. P. Singh, 2018. A Conceptual Framework for
Engineering Chatbots. IEEE Internet Computing 22, 54–59.
https://doi.org/10.1109/MIC.2018.2877827
P. Srivastava, N. Singh, 2020. Automatized Medical Chatbot (Medibot), in: 2020 International
Conference on Power Electronics & IoT Applications in Renewable Energy and Its Control
(PARC). Presented at the 2020 International Conference on Power Electronics & IoT Applications
in Renewable Energy and its Control (PARC), pp. 351–354.
https://doi.org/10.1109/PARC49193.2020.236624
Q. Xie, D. Tan, T. Zhu, Q. Zhang, S. Xiao, J. Wang, B. Li, L. Sun, P. Yi, 2019. Chatbot Application on
Cryptocurrency, in: 2019 IEEE Conference on Computational Intelligence for Financial
Engineering & Economics (CIFEr). Presented at the 2019 IEEE Conference on Computational
Intelligence for Financial Engineering & Economics (CIFEr), pp. 1–8.
https://doi.org/10.1109/CIFEr.2019.8759121
R. B. Mathew, S. Varghese, S. E. Joy, S. S. Alex, 2019. Chatbot for Disease Prediction and Treatment
Recommendation using Machine Learning, in: 2019 3rd International Conference on Trends in

Electronics and Informatics (ICOEI). Presented at the 2019 3rd International Conference on
Trends in Electronics and Informatics (ICOEI), pp. 851–856.
https://doi.org/10.1109/ICOEI.2019.8862707
R. Rajkumar, V. Ganapathy, 2020. Bio-Inspiring Learning Style Chatbot Inventory Using Brain
Computing Interface to Increase the Efficiency of E-Learning. IEEE Access 8, 67377–67395.
https://doi.org/10.1109/ACCESS.2020.2984591
R. Sahoo, B. R. Das, B. Kishore Mishra, 2020. Analysis and Implementation of Odia Part of Speech
Tagger in recent IoT based devices through Chatbot: A review, in: 2020 International Conference
on Computer Science, Engineering and Applications (ICCSEA). Presented at the 2020
International Conference on Computer Science, Engineering and Applications (ICCSEA), pp. 1–4.
https://doi.org/10.1109/ICCSEA49143.2020.9132940
S. -H. Lin, R. -S. Run, J. -Y. Yan, 2020. Chatbot Application in Laboratory Equipment Management and
e-assistant, in: 2020 International Symposium on Computer, Consumer and Control (IS3C).
Presented at the 2020 International Symposium on Computer, Consumer and Control (IS3C), pp.
39–42. https://doi.org/10.1109/IS3C50286.2020.00017
S. K. S, L. M. R, S. p. C, S. S. P, J. J. Paul, T. Bella Mary I, 2021. Chatbot - Attendance and Location
Guidance system (ALGs), in: 2021 3rd International Conference on Signal Processing and
Communication (ICPSC). Presented at the 2021 3rd International Conference on Signal
Processing and Communication (ICPSC), pp. 718–722.
https://doi.org/10.1109/ICSPC51351.2021.9451793
S. Punjabi, V. Sethuram, V. Ramachandran, R. boddu, S. Ravi, 2019. Chat bot Using API : Human To
Machine Conversation, in: 2019 Global Conference for Advancement in Technology (GCAT).
Presented at the 2019 Global Conference for Advancement in Technology (GCAT), pp. 1–5.
https://doi.org/10.1109/GCAT47503.2019.8978336
T. P. Nagarhalli, V. Vaze, N. K. Rana, 2020. A Review of Current Trends in the Development of Chatbot
Systems, in: 2020 6th International Conference on Advanced Computing and Communication
Systems (ICACCS). Presented at the 2020 6th International Conference on Advanced Computing
and Communication Systems (ICACCS), pp. 706–710.
https://doi.org/10.1109/ICACCS48705.2020.9074420
V. Selvi, S. Saranya, K. Chidida, R. Abarna, 2019. Chatbot and bullyfree Chat, in: 2019 IEEE
International Conference on System, Computation, Automation and Networking (ICSCAN).
Presented at the 2019 IEEE International Conference on System, Computation, Automation and
Networking (ICSCAN), pp. 1–5. https://doi.org/10.1109/ICSCAN.2019.8878779