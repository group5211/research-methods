Type of Research question: Late
Topic: CHATBOT AND DISABILITY
Research Question: What is the ideal chatbot framework for building an AI conversational chatbot to assist disabled people?

PICO elements:

Context: The chatbot must be quick, dependable, and effective in order to assist impaired persons. As a result, this research paper conducts trials in order to determine the best possible chatbot option for people with disabilities.

(P)problem: People currently suffer from a variety of disabilities and face numerous challenges in their daily lives. So, disabled people can use a chatbot to assist them in their daily duties. A voice assistant chatbot, for example, can assist a blind person in obtaining information. 

(I)intervention: To create a high-performance chatbot solution for disabled persons, we must compare various chatbot frameworks such as IBM Watson, DialogueFlow, and Microsoft Bot Framework using experiments to determine their efficiency. 

(C)comparison: A comparison of chatbot frameworks such as IBM Watson, DialogueFlow, and Microsoft Bot Framework that is currently available on the market. 

(O)outcome: The dependent variables obtained from the various chatbot frameworks comparison experiments include response time, dependability, clarity of responses, speech recognition speed, and voice recognition quality.
