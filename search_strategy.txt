Research Question: 	<What is the ideal chatbot framework for building an AI conversational chatbot to assist disabled people? >


Search String:		< (“chatbot framework” OR “chatbot machine” OR “chatbot simulator” OR “chatbot system” OR “chatbot application” OR chatterbot AND building OR create AND (“artificial intelligence” OR AI) AND (conversation OR chat OR dialogue OR discussion) AND (assist OR support OR aid OR help OR serve) AND disabled people) > (651 matches)




Dismissed Search Strings: - < (“Chatbot framework” OR “chatbot machine” OR “chatbot simulator” OR “Chatbot system” OR “chatbot application” OR chatterbot) AND building AND (“artificial intelligence” OR AI) OR (“natural language processing”) AND (convers* OR chat* OR dialogue OR disc*) AND (assist* OR support* OR aid OR help OR serve) AND (disabled OR incapacitated OR vulnerable) AND (people OR person* OR humans OR individuals) >



< (“chatbot framework” OR “chatbot machine” OR “chatbot simulator” OR “chatbot system” OR “chatbot application” OR chatterbot) AND building AND (“artificial intelligence” OR AI) AND (conversation OR chat OR dialogue OR discussion) AND (assist OR support OR aid OR help OR serve) AND disabled people >



< (“chatbot framework” OR “chatbot machine” OR “chatbot simulator” OR “chatbot system” OR “chatbot application” OR chatterbot) AND building AND (“artificial intelligence” OR AI) AND (convers* OR chat* OR dialogue OR disc*) AND (assist OR support OR aid OR help OR serve) AND “disabled people” >



< (“chatbot framework” OR “chatbot machine” OR “chatbot simulator” OR “chatbot system” OR “chatbot application” OR chatterbot) AND building AND (“artificial intelligence” OR “expert system” OR “machine learning” OR “knowledge engineering” OR “neural networks” OR “natural language processing”) AND (conversation OR chat OR dialogue OR discussion) AND assist AND disabled people >



Inclusion Criteria: -<Studies that include the chatbot framework for building an AI conversational chatbot to assist disabled people >


Exclusion criteria: -<Studies that does not state the chatbot framework required for building conversational chatbot>


Sources/search engines: 
•	IEEE Xplor
•	ACM Digital library
•	Scopus
